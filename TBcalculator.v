`timescale 1ns / 1ns
// Example testbench for calculator module
module calculatorTB;

	// Inputs of the module under test are created as type reg
	reg clk, rst, newkey;
	reg [4:0] keycode;

	// Outputs from the module under test are created as type wire
	wire [15:0] Xdisplay;
	
	// Define names for the non-digit keys
	// Note the leftmost bit of the keycode is inverted - see later
	localparam [4:0] 
        PLUS = 5'b11011, 
        MINUS = 5'b11010, 
        MULTIPLY = 5'b11001, 
        CLEAR = 5'b10100,
        ENTER = 5'b11100,
        CE = 5'b10011,
        SQUARED = 5'b10010;
	
	localparam CONSOLE = 1;  // file handle for console output
	
	// Testbench internal variables
	integer errorCount = 0;
	integer outFile;
	
	// Instantiate the Unit Under Test (UUT)
	myCalc UUT (
        .clk5 (clk),
        .rst (rst),
        .keycode (keycode),
		.newkey (newkey),
        .Q0 (Xdisplay)
		);

// Generate the 5 MHz clock signal
	initial begin
		clk = 0;		// initialise clock
		#100;		// delay at start
		forever
		  #100 clk = ~clk;		// delay 100 ns and invert the clock
	end
	
// Define the test sequence
	initial begin
    outFile = $fopen("calcTB_log.txt");        // open the log file

    rst = 1'b0;            // initialize all the inputs
    keycode = 5'b0;
    newkey = 1'b0;

    #100;               // delay before reset, so ouptut can be seen     
    rst = 1'b1;          // reset pulse of at least one clock cycle
    @(negedge clk);        // wait for falling clock edge
    @(negedge clk) rst = 1'b0;        // end pulse at second falling edge
    
    #200;            // more delay, so effect of reset can be seen
    CHECK(16'h0);    // output should be 0
    PRESS(4);          // press digit 4 key (task inverts bit 4 to get keycode)
    CHECK(16'h4);    // output should be 4
    PRESS(5);       // press  digit 5 key
    CHECK(16'h45);    // output should be 45 at this point
    PRESS(ENTER);    // press + key (task inverts bit 4 to get keycode)
    CHECK(16'h0);    // output should be 45 at this point
    PRESS(6);
    CHECK(16'h6);
    PRESS(PLUS);
    CHECK(16'h4B);
    PRESS(CLEAR);
    CHECK(16'h0); //Clear Previous
    //New entries: Stack should become 4 3 2 1 before operators
    PRESS(16'h1);
    CHECK(16'h1);
    PRESS(ENTER);
    CHECK(16'h0);
    PRESS(16'h2);
    CHECK(16'h2);
    PRESS(ENTER);
    PRESS(16'h3);
    PRESS(ENTER);
    PRESS(16'h4);
    PRESS(PLUS);
    CHECK(16'h7);
    PRESS(MULTIPLY);
    CHECK(16'hE);
    PRESS(MINUS);
    CHECK(16'hD);
    PRESS (CE);
    CHECK(16'h0);
    PRESS(16'h5);
    PRESS(SQUARED);
    CHECK(16'h19);
    PRESS(SQUARED);
	CHECK(16'h271);
    PRESS(SQUARED);
	CHECK(16'hF5E1);
    // continue to implement the verification plan...
    #600;     // wait to see the effect of the last action
    $fclose(outFile);  // close the log file
    $display("Simulation finished with %d errors",errorCount);
    $stop;            // stop the simulation
end


/*  Task to simulate input from the keypad.  Input to the task is a 5-bit value, 
    similar to the keycode, but with the MSB inverted.  This allows easier use 
    with digit keys.  The keypad hardware has outputs that change just after the
    rising edge of the clock, so this task will change the inputs to the calculator
    just after the rising edge of the clock.  */
	task PRESS (input [4:0] pseudoKeyCode);  
        begin
            @ (posedge clk);	// wait for clock edge
			#1 keycode = pseudoKeyCode ^ 5'h10;	// set keycode just after clock edge
			@ (posedge clk);	// wait for next clock edge
			#1 newkey = 1'b1;	// generate pulse on newkey, for one clock cycle
			@ (posedge clk);	
			#1 newkey = 1'b0;
			// log what has been pressed
			$fdisplay(outFile, "    time %t ps, key %h", $time, keycode);
			repeat(5)
				@ (posedge clk);    // hold the keycode for 5 more clock cycles
			#1 keycode = 5'h0;	    // then remove it
		end
	endtask

/*  Task to check the output from the calculator.  Input to this task is a 16-bit
    value that is the expected output of the calculator.  The output is checked
    just at the falling edge of the clock, when it should be stable.  All outputs 
    are logged.  Any errors are reported on the console and in the log file.  */ 
	task CHECK (input [15:0] expectedX);
        begin
            @ (negedge clk);	// wait for falling edge of clock
            if (Xdisplay != expectedX)	
                begin   // error message to log file and to console
                    $fdisplay(outFile|CONSOLE, "*** time %t ps, X = %h, expected %h", 
                                $time, Xdisplay, expectedX);
                    errorCount = errorCount + 1;	// increment error counter
                end
            else  // output as expected, just record it in the log file
                $fdisplay(outFile, "    time %t ps, X = %h", $time, Xdisplay);
        end
    endtask
		
endmodule
