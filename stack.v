module stack (
	input clk5,
	input rst,
	input [3:0] data,		//input from keyboard
	input [15:0] result, 	//calculator result
	input [2:0] select, 	//control logic input
	output [15:0] Q1_out,	//Second register output to ALU
	output [15:0] Q0_out 	//Bottom register output to ALU and display
);

wire [15:0] Q2_out, Q3_out;

// Instantiating each stack register

Stack_Register Q3(
	.clk5 (clk5),
	.above (16'd0),
	.below (Q2_out),
	.rst (rst),
	.select(select),
	.Q (Q3_out)
);

Stack_Register Q2(
	.clk5 (clk5),
	.above (Q3_out),
	.below (Q1_out),
	.rst (rst),
	.select(select),
	.Q (Q2_out)
);

Stack_Register Q1(
	.clk5 (clk5),
	.above (Q2_out),
	.below (Q0_out),
	.rst (rst),
	.select(select),
	.Q (Q1_out)
);

//Bottom Register distinct from stack register

Bottom_Register Q0(
	.clk5 (clk5),
	.above (Q1_out),
	.data (data),
	.result (result),
	.rst (rst),
	.select(select),
	.Q (Q0_out)
);

endmodule