module myCalc (
	input clk5,
	input rst,
	input newkey, 			//A key has been pressed when this is high
	input [4:0] keycode, 	//Keycode in binary from the keyboard
	output [15:0] Q0, 		//Output from bottom register
	output ovw 				//Overflow output from ALU
	);
	
	/* Declarations
	==========================================================*/
	
	wire [15:0]  result;
	wire [15:0]  Q1;
	reg  [2:0] 	 select;
	wire [4:0]   code;
    wire [2:0]   check;
    wire [3:0]   data;
    
	/* Instantiate Stack Module
	==========================================================*/
	
	stack stack4 (
		.clk5 	(clk5),
		.rst 	(rst),
		.data 	(data),
		.result (result),
		.select (select),
		.Q0_out (Q0),
		.Q1_out (Q1)
	);
	
/* Control Logic
	==========================================================*/
	
	assign code = newkey ? keycode : 5'd0; //newkey controls code multiplexer
	assign data = code[3:0]; //the last 4 bits of code determine the function
	
	localparam[4:0] //here I account for all the non digit possibilities of code
		    IDLE	= 	 5'b00000,
		   ENTER 	= 	 5'b01100
		    PLUS 	= 	 5'b01011,
           MINUS	= 	 5'b01010,
        MULTIPLY 	= 	 5'b01001,
		   CLEAR 	=  	 5'b00100,
		      CE	= 	 5'b00011,
         SQUARED 	= 	 5'b00010;

	always @ (code) //Look Up Table
		begin
			case(code)
				 IDLE 	  :    select = 3'b000; //Nothing entered
				ENTER     :    select = 3'b001; //Enter Pressed
				 PLUS 	  :    select = 3'b011; //Addition operator
				MINUS     :    select = 3'b011; //Subtraction operator
			 MULTIPLY 	  :	   select = 3'b011; //Multiply operator 
				CLEAR 	  :	   select = 3'b100; //AC operater 
				   CE     :    select = 3'b111; //Clear current entry operator
			  SQUARED     :    select = 3'b111; //Immediately square the current value of Q0
			 default	  :    select = 3'b110; //If none of the above: A digit has been entered
			endcase
		end

/* Instantiate Arithmetic Logic Block
	==========================================================*/
	alu alu (
		.Q0 		(Q0),
		.Q1		    (Q1),
		.operator 	(data), //the 4 bits of code are the operator as well as the data
		.result		({ovw,result}) //17 bit output of result is split into result and overflow, MSB is overflow bit
	);
	
endmodule	