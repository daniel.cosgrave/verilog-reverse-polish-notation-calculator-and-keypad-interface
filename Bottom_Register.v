module Bottom_Register (
	input clk5,
	input rst,
	input [15:0] above,
	input [3:0] data,
	input [15:0] result,
	input [2:0] select,
	output reg [15:0] Q
    );
	
    reg [15:0] in;
	localparam [2:0]
		   IDLE 	= 	3'b000,
		   PUSH		= 	3'b110,		
		    POP		= 	3'b010,
		CALCTWO 	= 	3'b011,
		   CALC 	= 	3'b111,
		     AC 	= 	3'b100,
		  ENTER 	= 	3'b001;
		

	always @ (posedge clk5)
			begin
				if(rst)
					Q <= 16'd0;
				else
					Q <= in;  
			end

	always @ (Q, above,data, select, result) //Multiplexer for bottom register... needs one extra input for clear current
		begin
		  case (select)
			 IDLE    :   in = Q; 				//Default is do nothing
			 PUSH    :   in = {Q[11:0],data}; 	//Shift Up
			  POP    :   in = above; 			//Shift Down
			 CALC    :   in = result; 			//Calculator operation requires Q1 and Q0
		  CALCTWO 	 :   in = result;			//Calculator operation requires just Q0
			   AC    :   in = 16'd0; 			//0 if clear all
			 ENTER   :   in = 16'd0;
		   default   : in = Q;
		  endcase
	   end
   
endmodule