module Stack_Register (
    input clk5,
	input [15:0]above,
	input [15:0]below,
	input rst,
	input [2:0] select,
	output reg [15:0] Q
);

    reg [15:0] in;
	
	localparam [2:0]
		 IDLE 	= 	3'b000,
		ENTER 	= 	3'b001,
		  POP	= 	3'b010,
	  CALCTWO   = 	3'b011,
		 CALC 	= 	3'b111,
		   AC 	= 	3'b100;
			

	always @ (posedge clk5)
			begin
				if(rst)
					Q <= 16'd0;
				else
					Q <= in;  
			end


		always @ (Q, above, below, select) //Multiplexer for stack input
	begin
      case (select)
            IDLE	: 	in = Q; 			//No input from keypad
           ENTER 	: 	in = below; 	//Enter has been pressed so shift up for new input
             POP 	: 	in = above; 		//Shift Down
         CALCTWO 	: 	in = above; 	//Calculator Input so shift down
            CALC   	: 	in = Q;
		      AC 	: 	in = 16'd0; 		//Clear all
		 default 	: 	in = Q; 		//by default do nothing
      endcase
   end
   
endmodule