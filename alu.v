module alu (
	input [15:0] Q0, Q1,
	input [3:0] operator,
	output reg [16:0] result
);

    localparam [3:0]
			 ADD    =	4'b1011,
        SUBTRACT    = 	4'b1010,
        MULTIPLY    = 	4'b1001,
			  CE    = 	4'b0011,
         SQUARED    = 	4'b0010;
        
    always @(Q0,Q1,operator)
        begin
            case(operator)
					 ADD	: 	result = Q0 + Q1;
                SUBTRACT	: 	result = Q1 - Q0;
                MULTIPLY	: 	result = Q0 * Q1;
					  CE	: 	result = 17'd0;
                 SQUARED	: 	result = Q0*Q0;
                 default	: 	result = Q0 + Q1;
            endcase
        end
    endmodule